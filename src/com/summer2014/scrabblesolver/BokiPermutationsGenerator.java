package com.summer2014.scrabblesolver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Boris Ivanovic on 8/9/2014.
 */
public class BokiPermutationsGenerator {

    public static List<String> permute(String str) {

        List<String> permutedStrings = new ArrayList<String>();

        if (str == null || str.isEmpty()) {
            return permutedStrings;
        }

        if (str.length() == 1) {
            return permutedStrings;
        }

        perm(str, "", "", permutedStrings);

        return permutedStrings;
    }

    private static void perm(String str, String already, String temp, List<String> permutedStrings){
        if (already.length() >= 1) {
            permutedStrings.add(already);
        }

        for (int i = 0; i < str.length(); i++){
            temp = str.substring(0, i) + str.substring(i + 1);
            perm(temp, already + str.charAt(i), "", permutedStrings);
        }
    }
}
