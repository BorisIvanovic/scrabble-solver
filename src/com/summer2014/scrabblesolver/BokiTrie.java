package com.summer2014.scrabblesolver;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Boris Ivanovic on 8/9/2014.
 */
public class BokiTrie {
    private TrieNode root;

    public BokiTrie() {
        root = new TrieNode(' ', "");
    }

    public void insert(String str) {
        insertAt(root, str.trim().toLowerCase(), "");
    }

    private void insertAt(TrieNode tn, String word, String prefix) {
        if (word.length() == 0)
            return;

        char ch = word.charAt(0);

        TrieNode child = null;
        for (TrieNode f : tn.list) {
            if (f.value == ch) {
                child = f;
                break;
            }
        }

        if (child == null) {
            child = new TrieNode(ch, prefix + ch);
            if (word.length() == 1) {
                child.isTerminal = true;
            }
            tn.list.add(child);
        }

        insertAt(child, word.substring(1), prefix + ch);
    }

    public void printAllWords() {
        printAt(root);
    }

    public ArrayList<String> getWordsByPrefix(String prefix) {
        ArrayList<String> words = new ArrayList<String>();

        if (prefix.length() == 0) {
            getWords(root, words);
            return words;
        }

        prefix = prefix.toLowerCase();

        getWordsByPrefix(root, words, prefix);

        for (String str : words) {
            System.out.printf("Word: %s%n", str);
        }
        return words;
    }

    private void getWords(TrieNode tn, ArrayList<String> words) {
        for (TrieNode child : tn.list) {
            if (child.isTerminal) {
                words.add(child.prefix);
            }
            getWords(child, words);
        }
    }

    private void getWordsByPrefix(TrieNode tn, ArrayList<String> words, String prefix) {
        if (prefix.isEmpty()) {
            getWords(tn, words);
            return;
        }

        char ch = prefix.charAt(0);

        for (TrieNode f : tn.list) {
            if (f.value == (ch)) {
                if (f.isTerminal && prefix.length() == 1)
                    words.add(f.prefix);
                getWordsByPrefix(f, words, prefix.substring(1));
            }
        }
    }

    private void printAt(TrieNode tn) {
        if (tn.isTerminal) {
            System.out.printf("Word: %s%n", tn.prefix);
        }

        for (TrieNode child : tn.list) {
            printAt(child);
        }
    }

    public void putAllWords(File listOfWordsFile) {
        try {
            long startTime = System.currentTimeMillis();
            Scanner scan = new Scanner(listOfWordsFile);
            int i = 0;
            while (scan.hasNextLine()) {
                this.insert(scan.nextLine());
                i++;
            }

            long endTime = System.currentTimeMillis();
            System.out.printf("Number of Words in this English Dictionary: %d%n", i);
            System.out.printf("Time Required to Load %d Words: %d ms %n%n", i, endTime - startTime);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean containsWord(String word) {
        return isWord(root, word);
    }

    private boolean isWord(TrieNode node, String word) {
        if (word.isEmpty())
            return node.isTerminal;

        char firstChar = word.charAt(0);
        String restOfWord = word.substring(1);

        for (TrieNode child : node.list) {
            if (child.value == firstChar)
                return isWord(child, restOfWord);
        }

        return false;
    }

//    public static void main(String[] args) {
//        BokiTrie bokiTrie = new BokiTrie();
//        bokiTrie.putAllWords(new File("officialscrabblewords.txt"));
//
//        System.out.println(bokiTrie.containsWord("abacuses"));
//    }
}

class TrieNode {

    protected char value;
    protected String prefix;
    protected boolean isTerminal = false;
    protected List<TrieNode> list;

    public TrieNode(char c, String prefix){
        value = c;
        this.prefix = prefix;
        list = new ArrayList<TrieNode>();
    }
}
