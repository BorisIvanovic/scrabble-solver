package com.summer2014.scrabblesolver;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Boris Ivanovic on 8/9/2014.
 */
public class ScrabbleSolver {

    private static final BokiPermutationsGenerator PERMUTATIONS_GENERATOR = new BokiPermutationsGenerator();

    private Map<Character, Integer> letterValueMap = new HashMap<Character, Integer>();
    private BokiTrie bokiTrie = new BokiTrie();

    public ScrabbleSolver(File scrabbleDictionaryFile){
        populateLetterValues(letterValueMap);
        bokiTrie.putAllWords(scrabbleDictionaryFile);
    }

    private void populateLetterValues(Map<Character, Integer> letterValueMap) {
        letterValueMap.put('e', 1);
        letterValueMap.put('a', 1);
        letterValueMap.put('i', 1);
        letterValueMap.put('o', 1);
        letterValueMap.put('n', 1);
        letterValueMap.put('r', 1);
        letterValueMap.put('t', 1);
        letterValueMap.put('l', 1);
        letterValueMap.put('s', 1);
        letterValueMap.put('u', 1);

        letterValueMap.put('d', 2);
        letterValueMap.put('g', 2);

        letterValueMap.put('b', 3);
        letterValueMap.put('c', 3);
        letterValueMap.put('m', 3);
        letterValueMap.put('p', 3);

        letterValueMap.put('f', 4);
        letterValueMap.put('h', 4);
        letterValueMap.put('v', 4);
        letterValueMap.put('w', 4);
        letterValueMap.put('y', 4);

        letterValueMap.put('k', 5);

        letterValueMap.put('j', 8);
        letterValueMap.put('x', 8);

        letterValueMap.put('q', 10);
        letterValueMap.put('z', 10);

        //2 blank (diamond) tiles (scoring 0 points)
        //1 point: E ×12, A ×9, I ×9, O ×8, N ×6, R ×6, T ×6, L ×4, S ×4, U ×4
        //2 points: D ×4, G ×3
        //3 points: B ×2, C ×2, M ×2, P ×2
        //4 points: F ×2, H ×2, V ×2, W ×2, Y ×2
        //5 points: K ×1
        //8 points: J ×1, X ×1
        //10 points: Q ×1, Z ×1
    }

    public List<WordPointPair> getAllPossibleWords(String userRack) {
        List<String> potentialWords = PERMUTATIONS_GENERATOR.permute(userRack);
        List<WordPointPair> correctWords = new ArrayList<WordPointPair>();
        WordPointPair pair;

        for (String word : potentialWords){
            if (bokiTrie.containsWord(word)){
                pair = new WordPointPair(word, getScoreForWord(word));
                if (!correctWords.contains(pair))
                    correctWords.add(pair);
            }
        }

        return correctWords;
    }

    private int getScoreForWord(String word) {
        int totalScore = 0;
        char c;
        for (int i = 0; i < word.length(); i++){
            c = word.charAt(i);
            totalScore += letterValueMap.get(c);
        }

        return totalScore;
    }

    public List<WordPointPair> getAllPossibleWordsWithPattern(String userRack, String patternString) {
        List<WordPointPair> correctWords = new ArrayList<WordPointPair>();

        StringBuilder stringBuilder = new StringBuilder();
        char c;
        for (int i = 0; i < patternString.length(); i++){
            c = patternString.charAt(i);
            if (c == '.') {
                stringBuilder.append("[a-z]");
            }
            else if (Character.isLetter(c)){
                stringBuilder.append(c);
                userRack += c;
            }
            else if (c == '*'){
                stringBuilder.append("[a-z]{0,}");
            }
        }

        List<WordPointPair> allWords = this.getAllPossibleWords(userRack);

        String regex = stringBuilder.toString();
        boolean hasStar = patternString.contains("*");

        for (WordPointPair pair : allWords){
            if ((hasStar || pair.getWord().length() == patternString.length()) && pair.getWord().matches(regex)){
                correctWords.add(pair);
            }
        }

        return correctWords;
    }

    public List<WordPointPair> getAllPossibleWordsOnLetters(String userRack, String lettersToPutWith) {
        List<WordPointPair> correctWords = new ArrayList<WordPointPair>();
        for (int i = 0; i < lettersToPutWith.length(); i++){
            correctWords.addAll(getAllPossibleWordsWithPattern(userRack, "*" + lettersToPutWith.charAt(i) + "*"));
        }

        return correctWords;
    }
}
