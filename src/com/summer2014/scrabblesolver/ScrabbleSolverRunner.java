package com.summer2014.scrabblesolver;

import java.io.File;
import java.util.Collections;
import java.util.Scanner;
import java.util.List;

/**
 * Created by Boris Ivanovic on 8/9/2014.
 */
public class ScrabbleSolverRunner {

    public static final int MAX_NUM_RACK_LETTERS = 7;

    public static void main(String[] args) {
        ScrabbleSolver solver = new ScrabbleSolver(new File("officialscrabblewords.txt"));

        System.out.print("What are the letters on your rack? Enter them all without commas or spaces: ");
        Scanner userInputScanner = new Scanner(System.in);
        String userRack = userInputScanner.nextLine();

        while (!validateInput(userRack)){
            System.out.print("Sorry, that is an invalid input. Please enter the letters on your rack again: ");
            userRack = userInputScanner.nextLine();
        }

        userRack = standardizeString(userRack);

        System.out.println("What pattern are you looking for, if any? Enter it like so:\n" +
                "\t\t\t . = any character in your rack\n" +
                "\tany letter = an already existing letter on the board in that spot of the word\n" +
                "\t\t\t * = any number of any characters in your rack\n" +
                "Or you may enter just a blank string by pressing the 'Enter' key. ");

        String patternString = userInputScanner.nextLine();
        while (!validateInput(userRack)){
            System.out.print("Sorry, that is an invalid input. Please enter the letters on your rack again: ");
            userRack = userInputScanner.nextLine();
        }
        patternString = standardizeString(patternString);

        String lettersToPutWith = "";
        if (patternString != null && patternString.isEmpty()){
            System.out.print("Which letters, if any (you could just press 'Enter' if none), would you like to try and " +
                    "place the words on the board with? Enter them all without commas or spaces: ");
            lettersToPutWith = userInputScanner.nextLine();
        }

        System.out.println();

        List<WordPointPair> possibleWordsAndScores = null;
        if (patternString != null && !patternString.isEmpty() && validatePattern(patternString)){
            possibleWordsAndScores = solver.getAllPossibleWordsWithPattern(userRack, patternString);
        }
        else if (!lettersToPutWith.isEmpty()){
            possibleWordsAndScores = solver.getAllPossibleWordsOnLetters(userRack, lettersToPutWith);
        }
        else {
            possibleWordsAndScores = solver.getAllPossibleWords(userRack);
        }

        if (possibleWordsAndScores == null || possibleWordsAndScores.isEmpty()){
            System.out.println("Sadly there are no words that you can form with your current rack and/or pattern :(");
            return;
        }

        Collections.sort(possibleWordsAndScores);

        System.out.println("Here are a list of possible words you can make, sorted by their point values:");

        for (WordPointPair toPrint : possibleWordsAndScores){
            System.out.println(toPrint.toString());
        }

        System.out.println("^ Most Points!!");
    }

    private static String standardizeString(String str) {
        return str.trim().toLowerCase();
    }

    private static boolean validatePattern(String patternString) {
        return true;
    }

    public static boolean validateInput(String userRackInput){
        if (userRackInput != null && userRackInput.length() <= MAX_NUM_RACK_LETTERS) {
            return true;
        }

        return false;
    }
}
