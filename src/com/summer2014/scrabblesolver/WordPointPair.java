package com.summer2014.scrabblesolver;

/**
 * Created by Boris Ivanovic on 8/9/2014.
 */
public class WordPointPair implements Comparable {

    private String word;
    private int score;

    public WordPointPair(String word, int score){
        this.word = word;
        this.score = score;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String toString(){
        return word + " " + score;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof WordPointPair){
            if (this.score < ((WordPointPair) o).getScore()) {
                return -1;
            }

            else if (this.score == ((WordPointPair) o).getScore()){
               return 0;
            }

            else {
                return 1;
            }
        }

        else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof WordPointPair){
            return (this.score == ((WordPointPair) obj).getScore() &&
                    this.word.equals(((WordPointPair) obj).getWord()));
        }
        else {
            return super.equals(obj);
        }
    }
}
